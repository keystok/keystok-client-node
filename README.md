# Keystok Node.js Client Library

This library is used to access your Keystok keys from your Node.js applications.

## Installation

    npm install keystok

## Retrieving a single key

    var keystok = require('keystok')('accesstoken');
    keystok.get('keyid', function(err, key) {
        console.log('key:', key);
    });

## Retrieving multiple keys

    var keystok = require('keystok')('accesstoken');
    keystok.get(['keyid1', 'keyid2', 'keyid3'], function(err, keys) {
        console.log('keys:', keys);
    });

## Retrieving all keys

    var keystok = require('keystok')('accesstoken');
    keystok.getAll(function(err, keys) {
        console.log('keys:', keys);
    });

## Options

When creating the Keystok client library object, you can specify either
an access token string or an options object. The options object can
specify these options (all except accessToken have default values):

    var keystok = require('keystok')({
        accessToken: 'accesstoken',
        apiHost: 'api.keystok.com',
        timeout: 5000,
        cache: true,
        cacheDir: '/tmp/keystok'
    });

To use the default options, just use this:

    var keystok = require('keystok')('accesstoken');

Note: The cacheDir option actually defaults to os.tmpdir() instead of '/tmp'.
