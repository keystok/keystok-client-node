/**
 * Keystok Client Library for Node.js
 *
 * Simple usage example:
 *
 * var keystok = require('keystok')('accesstoken');
 * keystok.get('keyid', function(err, key) {
 *   console.log('key:', key);
 * });
 */
var os = require('os');
var path = require('path');
var fs = require('fs');
var decryptKeys = require('./decrypt');
var apiRequest = require('./api');
var Cache = require('./cache');

var DEFAULT_API_HOST = 'https://api.keystok.com';
var DEFAULT_AUTH_HOST = 'https://keystok.com';

function keystok(accessTokenOrOptions) {
    var options = {};
    var cache = null;

    // Support either a simple access token or an options object.
    if (typeof accessTokenOrOptions === 'string') {
        options.accessToken = accessTokenOrOptions;
    } else {
        options = accessTokenOrOptions || {};
    }

    // Default options
    if (typeof options.apiHost === 'undefined') {
        options.apiHost = DEFAULT_API_HOST;
    }
    if (options.apiHost.slice(0, 4) !== 'http') {
        options.apiHost = 'https://' + options.apiHost;
    }
    if (typeof options.authHost === 'undefined') {
        options.authHost = DEFAULT_AUTH_HOST;
    }
    if (options.authHost.slice(0, 4) !== 'http') {
        options.authHost = 'https://' + options.authHost;
    }
    if (typeof options.timeout === 'undefined') {
        // 5 second timeout on API request (fallback to cache)
        options.timeout = 5000;
    }
    if (typeof options.cache === 'undefined') {
        // Enable cache by default
        options.cache = true;
    }
    if (typeof options.cacheDir === 'undefined') {
        // Put cache files in /tmp/keystok by default
        options.cacheDir = path.join(os.tmpdir(), 'keystok');
    }

    // Try to read access token from ./.keystok/access_token or ~/.keystok/access_token if not specified as an argument
    if (!options.accessToken) {
        try {
            var filename = path.join(process.cwd(), '.keystok', 'access_token');
            options.accessToken = fs.readFileSync(filename, {encoding:'utf8'}).trim();
        } catch (err) {
            // Ignore error
        }
    }
    if (!options.accessToken) {
        try {
            var filename = path.join(process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE, '.keystok', 'access_token');
            options.accessToken = fs.readFileSync(filename, {encoding:'utf8'}).trim();
        } catch (err) {
            // Ignore error
        }
    }
    if (!options.accessToken) {
        throw new Error('No Keystok access token specified');
    }

    if (options.cache) {
        // Create the cache manager
        cache = new Cache(options);
    }

    // Parse the access token's internal format
    try {
        options.tokenObj = JSON.parse(new Buffer(options.accessToken.replace(/-/g, '+').replace(/_/g, '/'), 'base64'));
    } catch (e) {
        throw new Error('Invalid access token');
    }
    // Expand shortened parameter names
    if (typeof options.tokenObj.id !== 'undefined') {
        options.tokenObj.app_id = options.tokenObj.id;
    }
    if (typeof options.tokenObj.at !== 'undefined') {
        options.tokenObj.access_token = options.tokenObj.at;
    }
    if (typeof options.tokenObj.rt !== 'undefined') {
        options.tokenObj.refresh_token = options.tokenObj.rt;
    }
    if (typeof options.tokenObj.dk !== 'undefined') {
        options.tokenObj.decryption_key = options.tokenObj.dk;
    }
    // Validity checks
    if (!options.tokenObj.refresh_token && !options.tokenObj.access_token) {
        throw new Error('Invalid access token (missing refresh_token or access_token)');
    }
    if (!options.tokenObj.app_id) {
        throw new Error('Invalid access token (missing app_id)');
    }
    if (!options.tokenObj.decryption_key) {
        throw new Error('Invalid access token (missing decryption_key)');
    }

    // If a refresh token is specified, ignore any access token (so that we get a fresh access token)
    if (options.tokenObj.refresh_token) {
        options.tokenObj.access_token = null;
    }

    function get(keyId, callback) {
        var keyStr = '';
        if (keyId) {
            keyStr = '/';
            if (Array.isArray(keyId)) keyStr += keyId.join(',');
            else keyStr += keyId;
        }
        apiRequest(options, '/apps/' + options.tokenObj.app_id + '/deploy' + keyStr, function(err, encryptedKeys) {
            if (err) {
                // Could not get key from network. Check if cache has the key.
                if (cache) {
                    cache.get(keyId, function(cachedKeys) {
                        if (cachedKeys !== null) {
                            // Found cached key, return it with no error.
                            decryptKeys(options, cachedKeys, callback);
                        } else {
                            // Could not find cached key, so return the original error
                            callback(err, null);
                        }
                    });
                } else {
                    // Cache not in use. Return error and no keys.
                    callback(err, null);
                }
            } else {
                if (cache) {
                    // Update cache with the key we got. Return after updating.
                    cache.set(keyId, encryptedKeys, function() {
                        // Return no error and decrypted keys.
                        decryptKeys(options, encryptedKeys, callback);
                    });
                } else {
                    // Return no error and decrypted keys.
                    decryptKeys(options, encryptedKeys, callback);
                }
            }
        });
    }

    // Return an object that can be used to query Keystok
    return {
        // Retrieve one key or multiple keys
        get: function(keyId, callback) {
            get(keyId, function(err, keys) {
                if (typeof keyId === 'string' && keys) {
                    // Individual key
                    callback(err, keys[keyId]);
                } else {
                    // Multiple keys (or error)
                    callback(err, keys);
                }
            });
        },

        // Retrieve all keys
        getAll: function(callback) {
            get(null, callback);
        },

        // List keys
        listKeys: function(callback) {
            apiRequest(options, '/apps/' + options.tokenObj.app_id + '/keys', function(err, keys) {
                callback(err, keys);
            });
        },

        // Clear cache
        clearCache: function(callback) {
            cache.clear(callback);
        },

        _options: options,
        _cache: cache
    };
}

module.exports = keystok;
