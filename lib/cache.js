var crypto = require('crypto');
var path = require('path');
var fs = require('fs');

var Cache = function(options) {
    this.options = options;
};

// The cache filename is sha1(app_id + ':' + keyid(s))
Cache.prototype.getCacheName = function(keyId) {
    var sha = crypto.createHash('sha1');
    sha.update(this.options.tokenObj.app_id + ':');
    if (typeof keyId === 'string') {
        sha.update(keyId);
    } else if (keyId) {
        sha.update(keyId.join(','));
    } else {
        sha.update('all');
    }
    return path.join(this.options.cacheDir, sha.digest('hex') + '.key');
};

// Get a cached key. Will call callback with null or the encrypted key data.
// Note that keyId can be also null or an array of keys.
Cache.prototype.get = function(keyId, callback) {
    var name = this.getCacheName(keyId);
    fs.readFile(name, function(err, data) {
        if (err) {
            // Could not read the file
            callback(null);
        } else {
            // Got data! Decode JSON
            try {
                var valueObj = JSON.parse(data);
                callback(valueObj);
            } catch (err) {
                // Error decoding data
                callback(null);
            }
        }
    });
};

// Set a cached key. Will call callback with error when done.
// Note that keyId can be also null or an array of keys.
Cache.prototype.set = function(keyId, valueObj, callback) {
    var name = this.getCacheName(keyId);
    fs.mkdir(this.options.cacheDir, 0770, function(err) {
        fs.writeFile(name, JSON.stringify(valueObj), function(err) {
            callback(err);
        });
    });
};

// Clear the whole cache of all keys.
Cache.prototype.clear = function(callback) {
    var that = this;
    fs.readdir(this.options.cacheDir, function(err, files) {
        for (var i = 0; i < files.length; i++) {
            if (files[i].match(/\.key$/)) {
                fs.unlinkSync(path.join(that.options.cacheDir, files[i]));
            }
        }
        callback();
    });
};

module.exports = Cache;
