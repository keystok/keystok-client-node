var crypto = require('crypto');

// Decrypt a single key, which looks like :aes256:<data>. The decryption key is in options.
function decrypt(options, encryptedKey) {
    var encryptedData, decryptedData;
    
    if (encryptedKey.slice(0, 8) == ':aes256:') {
        encryptedData = JSON.parse(new Buffer(encryptedKey.slice(8), 'base64').toString('utf8'));
    } else {
        throw new Error('Invalid key encryption scheme');
    }
    
    if (encryptedData.cipher == 'aes' &&
        encryptedData.mode == 'cbc' &&
        encryptedData.ks == 256 &&
        encryptedData.iv &&
        encryptedData.salt &&
        encryptedData.iter) {
        // Supported encryption mode: AES-256-CBC with IV and PBKDF2 / HMAC-SHA-256 derived key
        var iv = new Buffer(encryptedData.iv, 'base64');
        
        // this is the HMAC-SHA-1 based on Node.js internal crypto
         var salt = new Buffer(encryptedData.salt, 'base64');
         var derivedKey = crypto.pbkdf2Sync(options.tokenObj.decryption_key, salt, encryptedData.iter, 32);
        
        var ct = new Buffer(encryptedData.ct, 'base64');
        var cipher = crypto.createDecipheriv('aes-256-cbc', derivedKey, iv);
        decryptedData = cipher.update(ct);
        decryptedData += cipher.final();
    } else {
        throw new Error('Invalid encryption algorithm/parameter')
    }
    return decryptedData;
}

// Decrypt a key or multiple keys depending on type of encryptedKeys
function decryptKeys(options, encryptedKeys, callback) {
    if (typeof encryptedKeys === 'string') {
        // Decrypt single key
        try {
            var decryptedKey = decrypt(options, encryptedKeys);
            callback(null, decryptedKey);
        } catch (err) {
            return callback(err, null);
        }
    } else {
        // Decrypt multiple keys
        var decryptedKeys = {};
        for (var keyId in encryptedKeys) {
            if (encryptedKeys.hasOwnProperty(keyId)) {
                try {
                    decryptedKeys[keyId] = decrypt(options, encryptedKeys[keyId]);
                } catch (err) {
                    return callback(err, null);
                }
            }
        }
        return callback(null, decryptedKeys);
    }
}

module.exports = decryptKeys;
