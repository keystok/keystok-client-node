/**
 * OAuth2 refresh token support.
 */
var request = require('request');
var url = require('url');

// This function will request a new access token from OAuth2 and
// replace it into options.tokenObj.access_token if successful.
// The callback is called with err, which is null on success.
function requestRefreshedToken(options, callback) {
    if (!options.tokenObj.refresh_token) {
        // Can't request a new token without refresh token
        callback(new Error('Cannot refresh access token without refresh_token'));
        return;
    }
    var refreshUrl = url.resolve(options.authHost, '/oauth/token');
    request.post(refreshUrl, {
        form: {
            'grant_type': 'refresh_token',
            'refresh_token': options.tokenObj.refresh_token
        },
        json: true,
        timeout: options.timeout
    }, function(err, response, body) {
        if (err) {
            // Network error or timeout
            callback(err);
        } else if (response.statusCode != 200 || !body.access_token) {
            // HTTP error or invalid response
            callback(new Error('Invalid HTTP response to token refresh request'))
        } else {
            // Got new access token successfully. We will update it to the options.
            options.tokenObj.access_token = body.access_token;
            callback(null);
        }
    });
}

module.exports = requestRefreshedToken;
