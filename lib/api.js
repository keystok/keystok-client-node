var request = require('request');
var url = require('url');
var requestRefreshedToken = require('./refresh');

// This will execute the given API request.
// If the access token has expired, it will be refreshed and the API request is re-executed.
function doHttpRequest(alreadyRefreshed, reqUrl, options, httpOptions, callback) {
    if (!options.tokenObj.access_token) {
        // We don't have an access token to begin with, so we have to refresh it.
        if (alreadyRefreshed) {
            // Give up
            callback(new Error('Could not refresh OAuth2 access token'), null);
        } else {
            requestRefreshedToken(options, function(err) {
                if (err) {
                    // Refresh failed
                    callback(err, null);
                } else {
                    // Access token renewed, try again
                    doHttpRequest(true, reqUrl, options, httpOptions, callback);
                }
            });
        }
        return;
    }
    request(reqUrl + '?access_token=' + options.tokenObj.access_token, httpOptions,  function(err, response, body) {
        if (err) {
            return callback(err, null);
        }
        var code = Math.floor(response.statusCode / 100);
        if (!alreadyRefreshed && (response.statusCode == 400 || response.statusCode == 401)) {
            // Try to refresh the token and re-execute request
            requestRefreshedToken(options, function(err) {
                if (err) {
                    // Refresh failed
                    callback(err, null);
                } else {
                    // Access token renewed, try again
                    doHttpRequest(true, reqUrl, options, httpOptions, callback);
                }
            });
            return;
        }
        if (code != 2 && code != 3) {
            callback(new Error('Invalid HTTP response from server (' + code + ')'), null);
            return;
        }
        if (!body) {
            callback(new Error('Invalid data from server'), null);
            return;
        }
        try {
            // Make a dictionary of keys to key data
            var keys = {}
            if (Array.isArray(body)) {
                for (var i = 0; i < body.length; i++) {
                    keys[body[i].id] = body[i].key;
                }
            } else {
                for (var keyid in body) {
                    if (body.hasOwnProperty(keyid)) {
                        keys[keyid] = body[keyid].key;
                    }
                }
            }
            callback(null, keys);
        } catch (err) {
            // Decryption error
            callback(err, null);
        }
    });
}

// Make a Keystok API request to the server to get deployment keys
// Returns encrypted keys; the caller should decrypt them.
function apiRequest(options, reqPath, callback) {
    var reqUrl = url.resolve(options.apiHost, reqPath);
    doHttpRequest(false, reqUrl, options, {json:true, timeout:options.timeout}, callback);
}

module.exports = apiRequest;
