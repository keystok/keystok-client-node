var assert = require('chai').assert;
var decryptKeys = require('../lib/decrypt');

var ENCRYPTED_KEY = ':aes256:eyJpdiI6Ilkyb1lvUDYydlFXQlR1MmpXNVpZamc9PSIsInYiOjEsIml0ZXIiOjEwMDAsImtzIjoyNTYsInRzIjo2NCwibW9kZSI6ImNiYyIsImFkYXRhIjoiIiwiY2lwaGVyIjoiYWVzIiwic2FsdCI6ImNYa0g3N0RyUWs0PSIsImN0IjoiQm8yVHJhMVVjZEtOQTAzaWdXajJnZz09In0='
var DECRYPTION_KEY = '540c493c82ed2823bd594a83656ef57e268e3f4398d084981a117b0aab5243fa'

describe('Keystok decryption function', function() {
    it('should decrypt data successfully', function(done) {
        var encryptedData = { test_key_1: ENCRYPTED_KEY };
        decryptKeys({tokenObj:{decryption_key:DECRYPTION_KEY}}, encryptedData, function(err, decryptedData) {
            assert.isNull(err);
            assert.equal(decryptedData.test_key_1, 'Value N');
            done();
        });
    });
});
