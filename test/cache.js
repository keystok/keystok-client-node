var path = require('path');
var os = require('os');
var fs = require('fs');
var crypto = require('crypto');
var assert = require('chai').assert;
var server = require('./server');

describe('Keystok client cache', function() {
    beforeEach(function() {
        this.server = server();
    });

    it('should create cache file', function(done) {
        var keystok = require('../lib/keystok')(this.server.options);
        keystok.clearCache(function() {
            keystok.get('test_key_1', function(err, key) {
                assert.isNull(err);
                assert.equal(key, 'Hello World');
                // Let's verify the cache file was created
                var sha = crypto.createHash('sha1');
                sha.update('10:');
                sha.update('test_key_1');
                var filename = path.join(os.tmpdir(), 'keystok', sha.digest('hex') + '.key');
                assert.ok(fs.existsSync(filename), 'file ' + filename + ' exists');
                done();
            });
        });
    });

    it('should use cache file on timeout', function(done) {
        var keystok = require('../lib/keystok')({accessToken: this.server.options.accessToken, apiHost: '192.168.69.69:6969', timeout: 1000});
        keystok.get('test_key_1', function(err, key) {
            // This will fail unless the previously cached file is used
            assert.isNull(err);
            assert.equal(key, 'Hello World');
            done();
        });
    });
});
