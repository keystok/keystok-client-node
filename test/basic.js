var assert = require('chai').assert;
var path = require('path');
var os = require('os');
var fs = require('fs');
var KEYSTOK_TOKEN = require('./token');
var server = require('./server');

describe('Keystok client', function() {
    beforeEach(function() {
        this.server = server();
    });

    it('should instantiate', function(done) {
        var keystok = require('../lib/keystok')(this.server.options);
        keystok.clearCache(function() {
            assert.isNotNull(keystok);
            done();
        });
    });

    it('should not instantiate without access token', function(done) {
        var home = process.env.HOME;
        process.env.HOME = path.join(os.tmpdir(), 'temp-keystok-home');
        assert.throw(function() {
            var keystok = require('../lib/keystok')();
            assert.isNotNull(keystok);
        }, Error);
        process.env.HOME = home;
        done();
    });
    
    it('should instantiate with file access token', function(done) {
        var home = process.env.HOME;
        process.env.HOME = path.join(os.tmpdir(), 'temp-keystok-home');
        var tokendir = path.join(process.env.HOME, '.keystok');
        var tokenfile = path.join(tokendir, 'access_token');
        try { fs.mkdirSync(process.env.HOME); } catch (err) {}
        try { fs.mkdirSync(tokendir); } catch (err) {}
        fs.writeFileSync(tokenfile, KEYSTOK_TOKEN);
        var keystok = require('../lib/keystok')();
        assert.isNotNull(keystok);
        fs.unlinkSync(tokenfile);
        fs.rmdirSync(tokendir);
        fs.rmdirSync(process.env.HOME);
        process.env.HOME = home;
        done();
    });

    it('should retrieve a single key', function(done) {
        var keystok = require('../lib/keystok')(this.server.options);
        keystok.clearCache(function() {
            keystok.get('test_key_1', function(err, key) {
                assert.isNull(err);
                assert.equal(key, 'Hello World');
                done();
            });
        });
    });

    it('should retrieve multiple keys', function(done) {
        var keystok = require('../lib/keystok')(this.server.options);
        keystok.clearCache(function() {
            keystok.get(['test_key_1', 'test_key_2'], function(err, keys) {
                assert.isNull(err);
                assert.equal(keys.test_key_1, 'Hello World');
                assert.equal(keys.test_key_2, 'Foo Bar');
                done();
            });
        });
    });

    it('should retrieve all keys', function(done) {
        var keystok = require('../lib/keystok')(this.server.options);
        keystok.clearCache(function() {
            keystok.getAll(function(err, keys) {
                assert.isNull(err);
                assert.equal(keys.test_key_1, 'Hello World');
                assert.equal(keys.test_key_2, 'Foo Bar');
                done();
            });
        });
    });
});
