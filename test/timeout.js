var assert = require('chai').assert;
var KEYSTOK_TOKEN = require('./token');

describe('Keystok client request', function() {
    it('should timeout after 1 seconds', function(done) {
        var keystok = require('../lib/keystok')({accessToken: KEYSTOK_TOKEN, apiHost: '192.168.200.69:6969', timeout:1000 });
        keystok.get('test_key_1', function(err, key) {
            assert.isNotNull(err);
            done();
        });
    });
});


