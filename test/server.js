var http = require('http');
var querystring = require('querystring');
var urlmodule = require('url');
var KEYSTOK_TOKEN = require('./token');

module.exports = function() {
    // Start a mock server
    var server = http.createServer(function(req, res) {
        var url = urlmodule.parse(req.url);
        var data = '';
        req.on('data', function(chunk) {
            data += chunk;
        });
        req.on('end', function() {
            var m;
            var testkeys = {
                test_key_1: {key:':aes256:eyJpdiI6ImRPa2pCRGM4NGxDZnpOZ2V3L0NueXc9PSIsInYiOjEsIml0ZXIiOjEwMDAsImtzIjoyNTYsInRzIjo2NCwibW9kZSI6ImNiYyIsImFkYXRhIjoiIiwiY2lwaGVyIjoiYWVzIiwic2FsdCI6IjM0M2N0SmlMOTlZPSIsImN0IjoiZW0ycHVIeTVIMnUvTlR0ZTV1bmEzQT09In0='},
                test_key_2: {key:':aes256:eyJpdiI6IldvUFNRZm5wSEg1a0ZGZDBRcDJHZ1E9PSIsInYiOjEsIml0ZXIiOjEwMDAsImtzIjoyNTYsInRzIjo2NCwibW9kZSI6ImNiYyIsImFkYXRhIjoiIiwiY2lwaGVyIjoiYWVzIiwic2FsdCI6IjM0M2N0SmlMOTlZPSIsImN0IjoiWVpHNnRLRWhDNjV5TEQ2N2RqTENPQT09In0='}
            };
            var postdata = querystring.parse(data);
            if (url.pathname == '/oauth/token' && postdata.refresh_token == '0e0ab7c5ae5757deadbeef49b4b529ac44ddf9767f6300f1d5c3da9cc28a880e') {
                // Return new access token
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.end(JSON.stringify({
                    'access_token': '3b5d68065042a3ec1b3c71bf445f61497a00edce4621407f351b990e67e9fe49',
                    'token_type': 'bearer',
                    'expires_in': 86400
                }));
            } else if (m = url.pathname.match(/^\/apps\/(\w+)\/deploy\/(\w+)$/)) {
                // Deploy a key
                res.writeHead(200, 'OK', {'Content-Type': 'application/json'});
                var response = {};
                response[m[2]] = testkeys[m[2]];
                res.end(JSON.stringify(response));
            } else if (m = url.pathname.match(/^\/apps\/(\w+)\/deploy\/([\w,]+)$/)) {
                // Deploy multiple keys
                res.writeHead(200, 'OK', {'Content-Type': 'application/json'});
                var response = {};
                var keylist = m[2].split(',');
                for (var i = 0; i < keylist.length; i++) {
                    response[keylist[i]] = testkeys[keylist[i]];
                }
                res.end(JSON.stringify(response));
            } else if (m = url.pathname.match(/^\/apps\/(\w+)\/deploy$/)) {
                // Deploy all keys
                res.writeHead(200, 'OK', {'Content-Type': 'application/json'});
                var response = testkeys;
                res.end(JSON.stringify(response));
            } else {
                // Return error
                res.writeHead(400, 'Bad Request')
                res.end('Bad request');
            }
        })
    });
    server.listen();
    server.url = 'http://127.0.0.1:' + server.address().port + '/';
    server.options = {
        accessToken: KEYSTOK_TOKEN,
        apiHost: server.url,
        authHost: server.url
    };
    return server;
};
