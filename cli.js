#!/usr/bin/env node

// Keystok Command Line Interface (for Node.js)

function main() {
    var next = null;
    var accessToken = null;
    var cmd = null;
    var args = [];
    process.argv.forEach(function(arg, index) { if (index > 1) {
        if (next === 'a') {
            accessToken = arg;
            next = null;
        } else {
            if (arg.slice(0, 1) == '-') {
                next = arg.slice(1);
            } else {
                if (cmd === null) cmd = arg;
                else args.push(arg);
            }
        }
    }});
    if (!accessToken) {
        accessToken = process.env.KEYSTOK_ACCESS_TOKEN;
    }
    if (!accessToken) {
        console.log('Please specify an access token with -a or the KEYSTOK_ACCESS_TOKEN environment variable.');
        process.exit(1);
    }
    var keystok = require('./lib/keystok.js')(accessToken);
    if (cmd == 'ls') {
        keystok.listKeys(function(err, keys) {
            if (err) {
                sys.stderr.write(err.toString() + '\n');
                process.exit(1);
            } else {
                for (var key in keys) {
                    console.log(key);
                }
            }
        });
    } else if (cmd == 'get') {
        var keystr = args[0];
        if (keystr && keystr.indexOf(',') >= 0) keystr = keystr.split(',');
        keystok.get(keystr, function(err, key) {
            if (err) {
                sys.stderr.write(err.toString() + '\n');
                process.exit(1);
            } else {
                console.log(key);
            }
        });
    } else if (cmd === null) {
        console.log('Usage: keystok [-a token] command [args]');
        console.log('Commands:');
        console.log('  ls - List all keys');
        console.log('  get <keyid> - Get a key')
    } else {
        console.log('Unknown command "' + cmd + '"');
    }
}

main();
